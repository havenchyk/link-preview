# link-previewer
Simple link previewer

Supports
 - Partially Oembed format (for providers check ./lib/oembed/providers/providers.json)
 - Opengraph tags (check http://ogp.me/ for details)
 - Twitter cards (check https://dev.twitter.com/cards/overview for details)
 - meta tags as a fallback
