import bunyan from 'bunyan';

export default bunyan.createLogger({name: 'link-previewer'});
