import providers from './providers';
import got from 'got';
import log from './../log';

class OEmbed {
  constructor(url) {
    this.url = url;
  }

  getData() {
    //TODO: Rework this part in more generic way
    const oembedEndpoint = this.provider.oembedEndpoint;
    const encodedUrl = encodeURIComponent(this.url);

    const oembedURL = `${oembedEndpoint}?url=${encodedUrl}&maxwidth=400&format=json`;

    log.info(`oembedURL: ${oembedURL}`);

    return got.get(oembedURL).then(res => {
      return res.body;
    })
      .catch(function (err) {
        console.error(err);
        console.error(err.response && err.response.body);
      });
  }

  isSupported() {
    const isSupported = providers.some(provider => {

      const isSupported = this.url.includes(provider.url);

      if (isSupported) {
        this.provider = provider;
      }

      return isSupported;
    });

    log.info(`oembed provider supported: ${isSupported}`);

    return isSupported;
  }
}

export default OEmbed;
