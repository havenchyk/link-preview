import providers from './providers';
import URL from 'url';

//Use list of providers from github: https://github.com/iamcal/oembed/blob/master/providers.yml
// or from http://oembed.com/providers.json

const preparedProviders = providers.map(provider => {
  let url = provider.endpoints[0].url;

  return {
    url: URL.parse(provider.provider_url).host,
    oembedEndpoint: url && url.replace('{format}', 'json')
  };
});

export default preparedProviders;
