import Oembed from './oembed';
import Page from './page';

export default function (url) {
  let handler = new Oembed(url);

  if (!handler.isSupported()) {
    handler = new Page(url);
  }

  return handler;
};
