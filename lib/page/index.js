import got from 'got';
import cheerio from 'cheerio';
import URL from 'url';
import ogProperties from './opengraph-properties';
import twitterProperties from './twitter-properties';
import log from './../log.js';
import * as Utils from './utils';

class Page {
  constructor(url) {
    this.url = url;
  }

  downloadHtml() {
    return got.get(this.url).then(res => {
      const html = res.body;
      const $ = cheerio.load(html);

      this.$ = $;
      this.ogTags = $(Utils.SELECTORS.OPEN_GRAPH);
      this.twitterTags = $(Utils.SELECTORS.TWITTER);
    }).catch(function (err) {
      log.error(err);
      log.error(err.response && err.response.body);
    });
  }

  containsOpenGraphTags() {
    return this.ogTags.length > 0;
  }

  containsTwitterTags() {
    return this.twitterTags.length > 0;
  }

  getDataFromHtml() {
    const $ = this.$;
    const description = this.$(Utils.SELECTORS.PLAIN.DESCRIPTION).text();

    let data = {};

    if (description) {
      data.description = description;
    } else {
      const randomElement = $(Utils.SELECTORS.DESCRIPTION_FALLBACK);

      data.description = randomElement.first().text();
    }

    if (!data['thumbnail_url']) {
      let images = $(Utils.SELECTORS.PLAIN.IMAGE);

      let index = Math.floor(images.length / 2);

      if (images.length) {
        data['thumbnail_url'] = images[index].attribs.src;
      } else {
        let favicon = $(Utils.SELECTORS.PLAIN.FAVICON);

        if (favicon.length) {
          data['thumbnail_url'] = URL.parse(this.url).host + favicon.attr('href');
        } else {
          data['thumbnail_url'] = '';
        }
      }
    }

    data.title = $(Utils.SELECTORS.PLAIN.TITLE).text();
    data.url = this.url;
    data.html = '';
    data.type = 'link';//???TODO: use response type to detect type???

    return data;
  }

  getDataFromOG() {
    let data = {};

    [].forEach.call(this.ogTags, tag => {
      const name = tag.attribs.name || tag.attribs.property;
      const property = ogProperties[name];

      const field = property && property.field;

      if (field) {
        data[field] = tag.attribs.content;
      }
    });

    return Utils.normalize(data);
  }

  getDataFromTwitter() {
    let data = {};

    [].forEach.call(this.twitterTags, tag => {
      const name = tag.attribs.name;
      const property = twitterProperties[name];

      const field = property && property.field;

      if (field) {
        data[field] = tag.attribs.content;
      }
    });

    return Utils.normalize(data);
  }

  /**
   * Frontend expects to get. Mostly done
   * type
   * title
   * url
   * html
   * thumbnail_url
   */
  getData() {
    return this.downloadHtml().then(() => {
      let data = {};

      if (this.containsTwitterTags()) {
        log.info('Get data from twitter tags');
        data.twitter = this.getDataFromTwitter();
      }

      if (this.containsOpenGraphTags()) {
        log.info('Get data from opengraph tags');
        data.openGraph = this.getDataFromOG();
      }

      //TODO: be lazy. Invoke getData only if there are keys without values after previous steps
      log.info('Get plain info');
      data.plain = this.getDataFromHtml();

      data = Utils.merge(data);

      return data;
    });
  }
}

export default Page;
