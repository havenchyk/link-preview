export const KEYS = ['description', 'title', 'thumbnail_url', 'url', 'type'];
export const SOURCES = ['twitter', 'openGraph', 'plain'];

export function normalize(data) {
  let result = {};

  //Other properties are skipped for now
  result['description'] = data.description;
  result['title'] = data.title;
  result['thumbnail_url'] = data.image;
  result['url'] = data.url;
  result['type'] = data.type;

  return result;
}

export function merge(data) {
  //if something is undefined, try to get other info
  let result = {};

  SOURCES.forEach(source => {
    if (data[source]) {
      KEYS.forEach(key => {
        if (!result[key]) {
          result[key] = data[source][key];
        }
      });
    }
  });

  return result;
}

export const SELECTORS = {
  OPEN_GRAPH: 'meta[property^=og], meta[name^=og]',
  TWITTER: 'meta[name^=twitter]',
  PLAIN: {
    FAVICON: 'link[rel="shortcut icon"]',
    DESCRIPTION: 'meta[name=description]',
    DESCRIPTION_FALLBACK: '[class*=description]',
    IMAGE: 'img',
    TITLE: 'title'
  }
};
