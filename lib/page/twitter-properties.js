const supportedProperties = {
  'twitter:title': {
    multiple: false,
    field: 'title'
  },
  'twitter:description': {
    multiple: false,
    field: 'description'
  },
  'twitter:image': {
    multiple: true,
    field: 'image'
  },
  'twitter:image:height': {
    multiple: false,
    field: 'imageHeight'
  },
  'twitter:image:width': {
    multiple: true,
    field: 'imageWidth'
  },
  'twitter:url': {
    multiple: false,
    field: 'url'
  }
};

export default supportedProperties;
