const supportedProperties = {
  'og:title': {
    multiple: false,
    field: 'title'
  }, 'og:type': {
    multiple: false,
    field: 'type'
  }, 'og:image': {
    multiple: true,
    field: 'image'
  }, 'og:image:width': {
    multiple: true,
    field: 'imageWidth'
  }, 'og:image:height': {
    multiple: true,
    field: 'imageHeight'
  }, 'og:image:type': {
    multiple: true,
    field: 'imageType'
  }, 'og:url': {
    multiple: false,
    field: 'url'
  }, 'og:audio': {
    multiple: false,
    field: 'audio'
  }, 'og:description': {
    multiple: false,
    field: 'description'
  }, 'og:determiner': {
    multiple: false,
    field: 'determiner'
  }, 'og:locale': {
    multiple: false,
    field: 'locale'
  }, 'og:locale:alternate': {
    multiple: false,
    field: 'localeAlternate'
  }, 'og:site_name': {
    multiple: false,
    field: 'siteName'
  }, 'og:video': {
    multiple: true,
    field: 'video'
  }, 'og:video:width': {
    multiple: true,
    field: 'videoWidth'
  }, 'og:video:height': {
    multiple: true,
    field: 'videoHeight'
  }, 'og:video:type': {
    multiple: true,
    field: 'videoType'
  }
};

export default supportedProperties;
